# `wlclipper`

Clipboard persistence for wayland. Also has the ability to synchronize the
clipboards.

Planned features: paste notifications, clipboard history.

## Building

Just run `make`.

