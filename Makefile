O := build
CFLAGS += -I. -O2 -Wall -Wextra -pedantic -Wno-unused-parameter -Wno-unused-function

ifdef srcdir

all: wlclipper

wlclipper:   wlr-data-control-unstable-v1.o -lwayland-client
wlclipper.o: wlr-data-control-unstable-v1.h

vpath %.c   $(srcdir)
vpath %.xml $(srcdir)/protocol

%.c: %.xml
	wayland-scanner private-code $< $@
%.h: %.xml
	wayland-scanner client-header $< $@

.PHONY: all

else # boilerplate to have all files in a subdir

.DEFAULT_GOAL = all

Makefile: ;

%: | build
	@$(MAKE) --no-print-directory -C $O -f $(CURDIR)/Makefile srcdir=$(CURDIR) $@

$O:
	mkdir -p $@

clean:
	-rm -r $O

.PHONY: clean

endif

.SECONDARY:
.SUFFIXES:
.SUFFIXES: .o .c
