#define _POSIX_C_SOURCE 200809L // for strdup

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wayland-client.h>
#include <wlr-data-control-unstable-v1.h>
#include <getopt.h>

#define bail(message) do { fprintf(stderr, message "\n"); exit(1); } while (0)
// #define log(...) fprintf(stderr, __VA_ARGS__)
#define log(...)

// globals {{{

static struct registry *registry;
static struct wl_array/*struct sel_entry*/ *prev_selections[2] = {0};
static bool just_advertised[2] = {0};
static bool should_track[2] = {true, true};
static bool synchronize = false;
static char *paste_cmd = NULL;
static struct zwlr_data_control_device_v1 *zwlr_dcd;

static const struct wl_seat_listener wl_seat_listener;
static const struct zwlr_data_control_offer_v1_listener zwlr_dco_listener;
static const struct zwlr_data_control_device_v1_listener zwlr_dcd_listener;
static const struct zwlr_data_control_source_v1_listener zwlr_dcs_listener;

// }}} globals

// seat {{{

struct seat {
	struct wl_seat *wl_seat;
	char *name;
	uint32_t capabilities;
};

static void seat_destroy(struct seat *self) {
	wl_seat_destroy(self->wl_seat);
	free(self->name);
}

static void wl_seat_capabilities_handler(void *data, struct wl_seat *seat, uint32_t capabilities) {
	struct seat *self = data;
	self->capabilities = capabilities;
}

static void wl_seat_name_handler(void *data, struct wl_seat *seat, const char *name) {
	struct seat *self = data;
	self->name = strdup(name);
}

static const struct wl_seat_listener wl_seat_listener = {
	.name = wl_seat_name_handler,
	.capabilities = wl_seat_capabilities_handler,
};

// }}} seat

// registry {{{

struct registry {
	struct wl_display *wl_display;
	struct wl_registry *wl_registry;

	struct wl_array seats; // struct seat

	struct zwlr_data_control_manager_v1 *zwlr_data_control_manager_v1;
};

static void wl_registry_global_handler(void *data, struct wl_registry *wl_registry,
		uint32_t name, const char *interface, uint32_t version) {
	struct registry *self = data;

	if (strcmp(interface, zwlr_data_control_manager_v1_interface.name) == 0) {
		self->zwlr_data_control_manager_v1 =
			wl_registry_bind(wl_registry, name, &zwlr_data_control_manager_v1_interface, 2);
	} else if (strcmp(interface, wl_seat_interface.name) == 0) {
		struct seat *seat = wl_array_add(&self->seats, sizeof *seat);
		seat->wl_seat = wl_registry_bind(wl_registry, name, &wl_seat_interface, 2);
		wl_seat_add_listener(seat->wl_seat, &wl_seat_listener, seat);
	}
}

static void wl_registry_global_remove_handler(
	void *data, struct wl_registry *wl_registry, uint32_t name) {
}

static const struct wl_registry_listener wl_registry_listener = {
	.global = wl_registry_global_handler,
	.global_remove = wl_registry_global_remove_handler,
};

struct seat *registry_find_seat(struct registry *self, const char *name) {
	struct seat *seat;
	wl_array_for_each(seat, &self->seats) {
		if (name == NULL || strcmp(seat->name, name) == 0)
			return seat;
	}
	return NULL;
}

// }}} registry

// sel_entry {{{

struct sel_entry {
	char *mime_type;
	struct wl_array contents;
};

static void sel_entry_destroy(struct sel_entry *self) {
	free(self->mime_type);
	wl_array_release(&self->contents);
}

static void sel_entries_destroy(struct wl_array *entries) {
	struct sel_entry *entry;
	wl_array_for_each(entry, entries)
		sel_entry_destroy(entry);
	wl_array_release(entries);
}

static void sel_entries_free(struct wl_array *entries) {
	if (entries != NULL) {
		sel_entries_destroy(entries);
		free(entries);
	}
}

static void sel_entry_fill(struct sel_entry *self, int fd) {
	while (1) {
		// FIXME: kinda jank
		void *p = wl_array_add(&self->contents, PIPE_BUF);
		self->contents.size -= PIPE_BUF;
		ssize_t rx = read(fd, p, self->contents.alloc - self->contents.size);
		if (rx < 0)
			bail("failed reading thingy");
		self->contents.size += rx;
		if (rx == 0)
			break;
	}
}

static void sel_entries_fill(struct wl_array *entries, struct zwlr_data_control_offer_v1 *offer) {
	struct sel_entry *entry;
	wl_array_for_each(entry, entries) {
		int pipefd[2];
		if (pipe(pipefd) < 0)
			bail("failed creating pipe");
		zwlr_data_control_offer_v1_receive(offer, entry->mime_type, pipefd[1]);
		wl_display_roundtrip(registry->wl_display);
		close(pipefd[1]);
		sel_entry_fill(entry, pipefd[0]);
		close(pipefd[0]);
	}
}

static void sel_entries_advertise(bool is_primary) {
	log("Taking ownership of %s\n", is_primary ? "primary selection" : "clipboard");
	struct wl_array *entries = prev_selections[is_primary];
	if (entries == NULL)
		return;
	struct zwlr_data_control_source_v1 *src =
			zwlr_data_control_manager_v1_create_data_source(registry->zwlr_data_control_manager_v1);
	zwlr_data_control_source_v1_add_listener(src, &zwlr_dcs_listener, entries);
	struct sel_entry *entry;
	wl_array_for_each(entry, entries)
		zwlr_data_control_source_v1_offer(src, entry->mime_type);
	if (is_primary)
		zwlr_data_control_device_v1_set_primary_selection(zwlr_dcd, src);
	else
		zwlr_data_control_device_v1_set_selection(zwlr_dcd, src);
	just_advertised[is_primary] = true;
}

// }}} sel_entry

// data_control {{{

// data_control_offer {{{

static void zwlr_dco_offer(void *data, struct zwlr_data_control_offer_v1 *offer,
		const char *mime_type) {
	struct wl_array *entries = data;
	struct sel_entry *entry = wl_array_add(entries, sizeof *entry);
	memset(entry, 0, sizeof *entry);
	entry->mime_type = strdup(mime_type);
}

static const struct zwlr_data_control_offer_v1_listener zwlr_dco_listener = {
	.offer = zwlr_dco_offer,
};

// }}} data_control_offer

// data_control_device {{{

static void zwlr_dcd_data_offer(void *data, struct zwlr_data_control_device_v1 *device,
		struct zwlr_data_control_offer_v1 *offer) {
	struct wl_array *entries = calloc(1, sizeof *entries);
	zwlr_data_control_offer_v1_set_user_data(offer, entries);
	zwlr_data_control_offer_v1_add_listener(offer, &zwlr_dco_listener, entries);
}

static void zwlr_dcd_any_selection(void *data, struct zwlr_data_control_device_v1 *device,
		struct zwlr_data_control_offer_v1 *offer, bool is_primary) {
	struct wl_array *entries = NULL;
	if (offer != NULL)
		entries = zwlr_data_control_offer_v1_get_user_data(offer);

	if (!should_track[is_primary]) {
		sel_entries_free(entries);
		return;
	}

	if (just_advertised[is_primary]) {
		just_advertised[is_primary] = false;
		log("Ignoring own selection\n");
		sel_entries_free(entries);
		return;
	}

	if (entries == NULL) {
		sel_entries_advertise(is_primary);
	} else {
		sel_entries_fill(entries, offer);
		if (prev_selections[is_primary] != NULL)
			sel_entries_free(prev_selections[is_primary]);
		if (synchronize) {
			prev_selections[0] = prev_selections[1] = entries;
			sel_entries_advertise(0);
			sel_entries_advertise(1);
		} else {
			prev_selections[is_primary] = entries;
			sel_entries_advertise(is_primary);
		}
	}
}

static void zwlr_dcd_selection(void *data, struct zwlr_data_control_device_v1 *device,
		struct zwlr_data_control_offer_v1 *offer) {
	zwlr_dcd_any_selection(data, device, offer, false);
}

static void zwlr_dcd_primary_selection(void *data, struct zwlr_data_control_device_v1 *device,
		struct zwlr_data_control_offer_v1 *offer) {
	zwlr_dcd_any_selection(data, device, offer, true);
}

static void zwlr_dcd_finished(void *data, struct zwlr_data_control_device_v1 *device) {
	log("finished (probably need to die here)\n");
}

static const struct zwlr_data_control_device_v1_listener zwlr_dcd_listener = {
	.data_offer        = zwlr_dcd_data_offer,
	.selection         = zwlr_dcd_selection,
	.primary_selection = zwlr_dcd_primary_selection,
	.finished          = zwlr_dcd_finished,
};

// }}} data_control_device

// data_control_source {{{

static void paste_notify(bool is_primary, const char *mime_type) {
	log("Sending %s (%s)\n", is_primary ? "primary selection" : "clipboard", mime_type);
	// TODO run paste_cmd if it's not NULL
}

static void zwlr_dcs_send(void *data, struct zwlr_data_control_source_v1 *src,
		const char *mime_type, int32_t fd) {
	struct wl_array *entries = data;
	struct sel_entry *entry;
	wl_array_for_each(entry, entries) {
		if (strcmp(entry->mime_type, mime_type) == 0)
			break;
	}
	// FIXME handle nonexistant mime type
	paste_notify(entries == prev_selections[1], mime_type);
	size_t size = entry->contents.size;
	char *buf = entry->contents.data;
	while (size) {
		ssize_t tx = write(fd, buf, size);
		if (tx < 0)
			bail("write error");
		size -= tx;
		buf += tx;
	}
	close(fd);
}

static void zwlr_dcs_cancelled(void *data, struct zwlr_data_control_source_v1 *src) {
}

static const struct zwlr_data_control_source_v1_listener zwlr_dcs_listener = {
	.send = zwlr_dcs_send,
	.cancelled = zwlr_dcs_cancelled,
};

// }}} data_control_source

// }}} data_control

int main(int argc, char *const *argv) {
	int opt;
	bool regular = false, primary = false;
	char *seat_name = NULL;
	while (opt = getopt(argc, argv, "hcpbs:w:"), opt != -1) switch (opt) {
		case 'h':
			fprintf(stderr,
					"Usage: %s [OPTION]...\n"
					"    -h          Display this help and exit\n"
					"    -c          Operate on the regular clipboard only\n"
					"    -p          Operate on the primary selection only\n"
					"    -b          Keep clipboards synchronized\n"
					"    -s seat     Use the specified seat\n"
					"    -w cmd      Run given command when something gets pasted (TODO)\n"
					, argv[0]);
			return 0;
		case 'c':
			regular = true;
			break;
		case 'p':
			primary = true;
			break;
		case 'b':
			synchronize = true;
			break;
		case 's':
			seat_name = optarg;
			break;
		case 'w':
			paste_cmd = optarg;
			break;
		default:
			fprintf(stderr, "Try '%s -h' for more information.\n", argv[0]);
			return 1;
	}

	if (regular + primary + synchronize > 1)
		bail("Options 'c', 'p', and 'b' are mutually exclusive.");
	if (regular)
		should_track[1] = false;
	else if (primary)
		should_track[0] = false;

	struct wl_display *wl_display = wl_display_connect(NULL);
	if (wl_display == NULL)
		bail("Failed to connect to a Wayland server.");

	registry = &(struct registry) {0};

	registry->wl_display = wl_display;
	registry->wl_registry = wl_display_get_registry(wl_display);
	wl_registry_add_listener(registry->wl_registry, &wl_registry_listener, registry);

	wl_display_roundtrip(wl_display); // fill registry

	if (registry->zwlr_data_control_manager_v1 == NULL)
		bail("Server doesn't support the wlr-data-control-unstable-v1 protocol.");

	wl_display_roundtrip(wl_display); // get seat names

	struct seat *seat = registry_find_seat(registry, seat_name);
	if (seat == NULL)
		bail("No such seat.");

	zwlr_dcd = zwlr_data_control_manager_v1_get_data_device(
			registry->zwlr_data_control_manager_v1, seat->wl_seat);

	zwlr_data_control_device_v1_add_listener(zwlr_dcd, &zwlr_dcd_listener, NULL);

	while (wl_display_dispatch(wl_display) >= 0);

	perror("wl_display_dispatch");
	return 1;
}
